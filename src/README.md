# PyTech Rules

## Description
This project aims to provide tools to handle dynamic data structure conversions through rules definition.

The goal is to be able to define rules to translate data structs that may be incomplete and miss data.

## Roadmap
You may find some new features coming in the [Issue section](https://gitlab.com/pytech-srl/resources/pytech-rules/-/issues)

## Authors and acknowledgment
This package is provided thanks to:

- Alessandro Grandi (PyTech srl)
