==============================================
Welcome to PyTech Rules' documentation!
==============================================

********
Modules:
********

* :ref:`pytech.rules`


.. _pytech.rules:

pytech.rules
====

.. toctree::
   :maxdepth: 3
   :caption: pytech.rules

   pytech.rules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


|DocumentationVersion|