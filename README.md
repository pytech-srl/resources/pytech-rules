# PyTech Rules

![pipeline status](https://gitlab.com/pytech-srl/resources/pytech-rules/badges/main/pipeline.svg)
![coverage status](https://gitlab.com/pytech-srl/resources/pytech-rules/badges/main/coverage.svg)
![interrogate status](https://gitlab.com/pytech-srl/resources/pytech-rules/-/raw/main/interrogate_badge.svg)

## Description
This project aims to provide tools to handle dynamic data structure conversions through rules definition.

The goal is to be able to define rules to translate data structs that may be incomplete and miss data.

## Roadmap
You may find some new features coming in the [Issue section](https://gitlab.com/pytech-srl/resources/pytech-rules/-/issues)

## Authors and acknowledgment
This package is provided thanks to:

- Alessandro Grandi (PyTech srl)
