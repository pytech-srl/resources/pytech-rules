import pytest

from pytech.rules.conversions import _rotate_angle_by, safe_stdev


@pytest.mark.parametrize(
    ["rotation", "data", "expected_result"],
    [
        [0, -90, 270],
        [0, -180, 180],
        [0, 0, 0],
        [0, 90, 90],
        [0, 360, 0],
        [180, 0, 180],
        [180, 90, 270],
        [180, 180, 0],
        [180, -1, 179],
        [180, -180, 0],

    ]
)
def test__rotate_angle_by(rotation, data, expected_result):
    assert _rotate_angle_by(rotation)(data) == expected_result


@pytest.mark.parametrize(
    ["data", "expected_result"],
    [
        [[1, 2, 3], 1.0],
        [[1], 0.0]

    ]
)
def test__safe_stddev(data, expected_result):
    assert safe_stdev(data) == expected_result
