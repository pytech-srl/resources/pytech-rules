import pytest

from pytech.rules.conversions import no_conversion
from pytech.rules.rules import validate_rules, Rule


@pytest.mark.parametrize("wrong_param", [0, 1.2, "a_string", [1], {1}, None, True])
def test__raises_type_error_if_not_dict(wrong_param):

    with pytest.raises(TypeError):
        validate_rules(wrong_param)


@pytest.mark.parametrize(
    "wrong_param",
    [0, 1.2, "a_string", [1], {1}, {1: 2}, None, True]
)
def test__raises_type_error_if_dict_does_not_contain_rules(wrong_param):

    wrong_rules = {1: wrong_param}

    with pytest.raises(TypeError):
        validate_rules(wrong_rules)


@pytest.mark.parametrize(
    "wrong_param",
    [0, 1.2, "a_string", {1}, {1: 2}, None, True]
)
def test__raises_type_error_if_rule_fields_is_not_a_list(wrong_param):

    wrong_rules = {1: Rule(fields=wrong_param, convert=no_conversion)}

    with pytest.raises(TypeError):
        validate_rules(wrong_rules)


@pytest.mark.parametrize(
    "empty_param",
    [[], tuple()]
)
def test__raises_value_error_if_rule_has_empty_fields(empty_param):
    wrong_rules = {1: Rule(fields=empty_param, convert=no_conversion)}

    with pytest.raises(ValueError):
        validate_rules(wrong_rules)


@pytest.mark.parametrize(
    "wrong_param",
    [0, 1.2, "a_string", {1}, {1: 2}, None, True]
)
def test__raises_value_error_if_rule_convert_is_not_callable(wrong_param):
    wrong_rules = {1: Rule(fields=[1], convert=wrong_param)}

    with pytest.raises(ValueError):
        validate_rules(wrong_rules)


@pytest.mark.parametrize("any_key", [0, 1.2, "a_string", (1,), None, True])
def test__accept_a_dict_containing_valid_rules(any_key):

    rules = {any_key: Rule(fields=[100], convert=no_conversion)}
    validate_rules(rules)
