import pytest

from pytech.rules.conversions import no_conversion
from pytech.rules.rules import Rule, apply_rules


@pytest.mark.parametrize(
    ["rules", "data", "expected_result"],
    [
        [
            {"A": Rule(fields=[0], convert=no_conversion)},
            {0: 42},
            {"A": 42}
        ],
        [
            {"A": Rule(fields=[1, 2], convert=lambda a, b: a + b)},
            {1: "x", 2: "y"},
            {"A": "xy"}
        ],
        [
            {"A": Rule(fields=[1, 2], convert=lambda a, b: a + b)},
            {1: 1, 2: 2},
            {"A": 3}
        ],
    ]
)
def test_results_for_apply_rules(rules, data, expected_result):
    assert expected_result == apply_rules(rules=rules, data=data)
